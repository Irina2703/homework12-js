'use strict'


/* 
Теоретичні питання
1. Як можна визначити, яку саме клавішу клавіатури натиснув користувач?

    Щоб визначити, яку саме клавішу клавіатури натиснув користувач, в JavaScript можна використати об'єкт події event, що передається в обробник подій "keydown" (або "keyup"). Один з атрибутів цього об'єкта — це event.code, який містить фізичний код клавіші.


2. Яка різниця між event.code() та event.key()?

    Метод event.code та властивість event.key — це два різних способи отримання інформації про клавішу, яку натиснув користувач.
    event.code повертає фізичний код клавіші, що натиснута. Цей код зазвичай відображає розташування клавіші на клавіатурі незалежно від різних регіональних налаштувань клавіатури.
    event.key повертає відображення символу, який відповідає натиснутій клавіші з урахуванням поточної мови клавіатури та можливих модифікаторів (наприклад, Shift).
    Отже, основна різниця між event.code та event.key полягає в тому, що перший повертає фізичний код клавіші, а другий — відображення символу, який був натиснутий, з урахуванням різних умов та налаштувань.

3. Які три події клавіатури існує та яка між ними відмінність?  
    keypress застосування  події обмежувалося тим, що воно не було стійким і не працювало однаково в усіх браузерах. У зв'язку з цим, з часом keypress було вважено застарілим і не рекомендовано до використання.


Практичне завдання.
Реалізувати функцію підсвічування клавіш.

Технічні вимоги:

- У файлі index.html лежить розмітка для кнопок.
- Кожна кнопка містить назву клавіші на клавіатурі
- Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною. Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.
*/


window.addEventListener("keydown", (event) => {
    const activeElem = document.querySelector(".active");
    if (activeElem) {
        activeElem.classList.remove("active");

    }
    const el = document.querySelector(`#${event.key.toLowerCase()}-item`);
    console.log(el);
    if (el) {
        el.classList.add("active");
    }
})



